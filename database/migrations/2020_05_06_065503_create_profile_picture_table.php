<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilePictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_picture', function (Blueprint $table) {
            $table->unsignedBigInteger('picture_id');
            $table->unsignedBigInteger('profile_id');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')->onDelete('cascade');
            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picture_profile');
    }
}
