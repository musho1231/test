import ProfilesListComponent from './components/ProfilesListComponent.vue';
import CreateProfilesListComponent from './components/CreateProfilesListComponent';
import EditProfilesListComponent from './components/EditProfilesListComponent';

const routes = [
    { path: '/', name:"profiles", component: ProfilesListComponent },
    { path: '/creat-profile', name:"creat_profile", component: CreateProfilesListComponent },
    { path: '/profile/:id', name:"edit_profile", component: EditProfilesListComponent }
];

export default routes;
