/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


import {store} from './store';
import routes from './routes';
import vueRouter from 'vue-router'
import { CardPlugin } from 'bootstrap-vue'
import { NavbarPlugin } from 'bootstrap-vue'
import { ButtonPlugin } from 'bootstrap-vue'
import { FormPlugin } from 'bootstrap-vue';
import { FormGroupPlugin } from 'bootstrap-vue'
import { FormInputPlugin } from 'bootstrap-vue'
import { TablePlugin } from 'bootstrap-vue'
import { FormFilePlugin } from 'bootstrap-vue'
import { AlertPlugin } from 'bootstrap-vue'

Vue.use(AlertPlugin)
Vue.use(FormFilePlugin)
Vue.use(TablePlugin)
Vue.use(FormInputPlugin)
Vue.use(FormGroupPlugin)
Vue.use(FormPlugin)
Vue.use(ButtonPlugin);
Vue.use(CardPlugin);
Vue.use(NavbarPlugin);


Vue.use(vueRouter);
Vue.component('appComponent', require('./components/AppComponent.vue').default);
const router = new vueRouter({
    routes: routes
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
});
