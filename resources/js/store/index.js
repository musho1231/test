import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        profiles: [],
    },

    getters: {
        PROFILES: state => {
            return state.profiles;
        },
    },

    mutations: {
        SET_PROFILES: (state, payload) => {
            state.profiles = payload;
        },

        ADD_PROFILE: (state, payload) => {
            state.profiles.push(payload);
        },

        REMOVE_PROFILE: (state, payload) => {
            let filteredProfiles = state.profiles.filter(p => {
                return p.id !== payload
            });
            state.profiles = filteredProfiles;
        },
    },

    actions: {
        GET_PROFILES: async (context, payload) => {
            let {data} = await Axios.get('api/profiles');
            context.commit('SET_PROFILES', data.data);
        },

        EDIT_PROFILES: async (context, payload) => {
            let {data} = await Axios.put('api/profiles/' + payload.id, payload.data);
        },

        SAVE_PROFILE: async (context, payload) => {
            let {data} = await Axios.post('api/profiles',payload);
            context.commit('ADD_PROFILE', data.date);
        },

        DELETE_PROFILES: async (context, payload) => {
            let {data} = await Axios.delete('api/profiles/'+ payload.id);
            context.commit('REMOVE_PROFILE', payload.id);
        },
    },
});
