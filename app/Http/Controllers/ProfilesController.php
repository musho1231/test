<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Resources\Profile as ProfileResource;
use App\Picture;
use Illuminate\Http\Request;
use App\Profile;
class ProfilesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProfileResource::collection(Profile::orderBy('created_at', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param ProfileRequest $request
     */
    public function store(ProfileRequest $request)
    {
        $data = $request->all();

        $profile = Profile::create($data);
        if ($data['images']) {
            $profile->pictures()->sync($data['images']);
        }

        return response()->json([
            "date" => $profile->load('pictures')
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return new ProfileResource($profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('name');

        Profile::where('id', $id)->update($data);
        if ($request->has('images')) {
            $profile = Profile::find($id);
            $profile->pictures()->sync($request->images);
        }

        return response()->json([
            "date" => $profile->load('pictures')
        ],200);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $profile = Profile::findOrFail($id);
        $profile->delete();
        return response()->json([
            "status" => true
        ],200);
    }
}
