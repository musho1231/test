<?php

namespace App\Http\Controllers;

use App\Http\Requests\PictureRequest;
use App\Picture;

class PictureController extends Controller
{
    public function store(PictureRequest $request)
    {
        if ($request->hasFile('images')) {
            $images = $request->file('images');
            $pictures = [];
            $picture_ids = [];
            foreach ($images as $image) {
                $fileName = uniqid() . '.' . $image->guessClientExtension();
                $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images';
                $image->move($destinationPath, $fileName);
                $picture = Picture::create([
                    "url" => "/images/".$fileName,
                ]);
                array_push($picture_ids, $picture->id);
                array_push($pictures, $picture);
            }
        }
        if ($pictures){
            return response()->json([
                'data' => $pictures,
                'picture_ids' => $picture_ids
            ], 200);
        }
        return response()->json([
            'status' => false
        ], 403);
    }

    public function destroy($id)
    {
        $profile = Picture::findOrFail($id);
        $profile->delete();
        return response()->json([
            "status" => true
        ],200);
    }
}
