<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = ['profile_id', 'url'];

    public function profile() {
        return $this->belongsTo(Profile::class);
    }

    public function pictures()
    {
        return $this->belongsToMany(Profile::class,"profile_picture", "picture_id", "profile_id");
    }

}
