<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['name'];
    protected $with = ['pictures'];

    public function pictures()
    {
        return $this->belongsToMany(Picture::class,"profile_picture");
    }

}
